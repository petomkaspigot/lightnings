package me.petomka.lightnings.commands;

import com.google.common.collect.ImmutableList;
import me.petomka.lightnings.LightningMain;
import me.petomka.lightnings.listeners.PlayerListener;
import me.petomka.lightnings.math.Lightning;
import me.petomka.lightnings.util.ConfigModel;
import me.petomka.lightnings.util.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.stream.Collectors;

public class LightningsCommand implements CommandExecutor, TabExecutor {

	private final LightningMain lightningMain;
	private final PlayerListener playerListener;

	public LightningsCommand(LightningMain lightningMain, PlayerListener playerListener) {
		this.lightningMain = lightningMain;
		this.playerListener = playerListener;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
		if (args.length == 0) {
			return false;
		}

		if (args[0].equalsIgnoreCase("give")) {
			onGive(sender, args);
			return true;
		}

		if (args[0].equalsIgnoreCase("reload")) {
			onReload(sender);
			return true;
		}

		return true;
	}

	private void onGive(CommandSender sender, String[] args) {
		if (!sender.hasPermission("lightnings.give")) {
			Message.NO_PERMISSION.send(sender);
			return;
		}
		Player target = null;
		if (args.length == 1 && sender instanceof Player) {
			target = (Player) sender;
		}
		if (args.length == 2 && (target = Bukkit.getPlayerExact(args[1])) == null) {
			Message.PLAYER_NOT_FOUND.send(sender, "{player}", args[1]);
			return;
		}
		if (target == null) {
			Message.SPECIFY_PLAYER.send(sender);
			return;
		}
		Inventory targetInv = target.getInventory();
		if (targetInv.firstEmpty() == -1) {
			Message.NO_FREE_INVENTORY_SLOT.send(sender);
			return;
		}
		ItemStack lightningItem = new ItemStack(ConfigModel.getItemType());
		ItemMeta meta = lightningItem.getItemMeta();
		meta.setDisplayName(ConfigModel.getItemName());
		lightningItem.setItemMeta(meta);
		targetInv.addItem(lightningItem);
		Message.ITEM_GIVEN.send(sender, "{player}", target.getName());
	}

	private void onReload(CommandSender sender) {
		if (!sender.hasPermission("lightnings.reload")) {
			Message.NO_PERMISSION.send(sender);
			return;
		}
		Message.LIGHTNINGS_RELOADING.send(sender);
		lightningMain.reloadConfig();
		ConfigModel.addDefaults(lightningMain);
		playerListener.reloadVariables();
		Lightning.setDIST_BETWEEN(ConfigModel.getDistBetween());
		Message.LIGHTNINGS_RELOADED.send(sender);
	}

	@Override
	public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
		String lastArg = args[args.length - 1];
		if (args.length == 2 && args[0].equalsIgnoreCase("give")) {
			return Bukkit.getOnlinePlayers().stream()
					.map(Player::getName)
					.filter(name -> name.startsWith(lastArg))
					.collect(Collectors.toList());
		}
		return ImmutableList.of("reload", "give");
	}
}
