package me.petomka.lightnings.listeners;

import lombok.Getter;
import me.petomka.lightnings.LightningMain;
import me.petomka.lightnings.events.LightningDamageEvent;
import me.petomka.lightnings.events.LightningFireEvent;
import me.petomka.lightnings.math.DefaultLightningCreator;
import me.petomka.lightnings.math.Lightning;
import me.petomka.lightnings.util.ColorTransition;
import me.petomka.lightnings.util.ConfigModel;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.util.Vector;

import java.util.Collection;
import java.util.Set;

public class PlayerListener implements Listener {

	private LightningMain lightningMain;

	private double length;
	private double lengthMod;
	private int maxChain;
	private int maxLightnings;
	private double maxJitter;
	private double jitterMod;
	private double splitChance;
	private double splitMod;
	private int rgbStart;
	private int rgbEnd;
	private Material itemType;
	private String itemName;
	private String permission;
	private double lightningDamage;
	@Getter
	private static double damageRadius;

	public PlayerListener(LightningMain lightningMain) {
		this.lightningMain = lightningMain;
		reloadVariables();
	}

	public void reloadVariables() {
		length = ConfigModel.getLength();
		lengthMod = ConfigModel.getLengthMod();
		maxChain = ConfigModel.getMaxChain();
		maxLightnings = ConfigModel.getMaxLightnings();
		maxJitter = ConfigModel.getMaxJitter();
		jitterMod = ConfigModel.getJitterMod();
		splitChance = ConfigModel.getSplitChance();
		splitMod = ConfigModel.getSplitMod();
		rgbStart = ConfigModel.getRGBStart();
		rgbEnd = ConfigModel.getRGBEnd();
		itemType = ConfigModel.getItemType();
		itemName = ConfigModel.getItemName();
		permission = ConfigModel.getPermission();
		lightningDamage = ConfigModel.getLightningDamage();
		damageRadius = ConfigModel.getDamageRadius();
	}

	@EventHandler
	public void onRightclick(PlayerInteractEvent event) {
		if (event.getHand() != EquipmentSlot.HAND) {
			return;
		}
		if (!(event.getAction() == Action.RIGHT_CLICK_AIR
				&& event.getItem() != null
				&& event.getItem().hasItemMeta()
				&& event.getItem().getItemMeta().hasDisplayName()
				&& event.getItem().getType() == itemType
				&& event.getItem().getItemMeta().getDisplayName().equals(itemName)
				&& event.getPlayer().hasPermission(permission))) {
			return;
		}
		LightningFireEvent fireEvent = new LightningFireEvent(event.getPlayer());
		Bukkit.getPluginManager().callEvent(fireEvent);
		if (fireEvent.isCancelled()) {
			return;
		}
		final Vector dir = event.getPlayer().getLocation().getDirection().normalize();
		Bukkit.getScheduler().runTaskAsynchronously(lightningMain, () -> {
			Collection<Lightning> lightnings = DefaultLightningCreator.createLightning(
					event.getPlayer().getWorld(),
					event.getPlayer().getLocation().add(0, 1, 0),
					dir.clone(),
					length, lengthMod, maxChain, maxLightnings,
					maxJitter, jitterMod, splitChance, splitMod
			);
			Bukkit.getScheduler().callSyncMethod(lightningMain, () -> {
				Set<Entity> entities = Lightning.create(lightnings, Lightning.NO_LIMIT, new ColorTransition(rgbStart, rgbEnd,
						Lightning.countTotalLightnings(lightnings)));
				if (lightningDamage < 0.0) {
					return null;
				}
				LightningDamageEvent damageEvent = new LightningDamageEvent(entities);
				Bukkit.getPluginManager().callEvent(damageEvent);
				if (damageEvent.isCancelled()) {
					return null;
				}
				damageEvent.getEntities().stream()
						.filter(entity -> !entity.equals(event.getPlayer()))
						.filter(entity -> entity instanceof LivingEntity)
						.filter(entity -> !(entity instanceof Player)
								|| ((Player) entity).getGameMode() != GameMode.CREATIVE)
						.forEach(entity -> {
							EntityDamageEvent entityDamageEvent = new EntityDamageEvent(event.getPlayer(),
									EntityDamageEvent.DamageCause.LIGHTNING, lightningDamage);
							Bukkit.getPluginManager().callEvent(entityDamageEvent);
							if (!entityDamageEvent.isCancelled()) {
								((LivingEntity) entity).damage(lightningDamage);
								entity.setLastDamageCause(entityDamageEvent);
							}
						});
				return null;
			});
		});
	}

}
