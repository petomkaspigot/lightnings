package me.petomka.lightnings.util;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import lombok.Getter;
import lombok.experimental.UtilityClass;
import me.petomka.lightnings.LightningMain;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

import javax.annotation.Nonnull;
import java.math.BigInteger;
import java.util.Optional;

@UtilityClass
public class ConfigModel {

	private static LightningMain main;

	public static void addDefaults(@Nonnull LightningMain main) {
		Preconditions.checkNotNull(main, "main");
		if (ConfigModel.main == null) {
			ConfigModel.main = main;
		}
		setConfig(main.getConfig());
		config.addDefault("math.length", 1.2);
		config.addDefault("math.lengthMod", 1);
		config.addDefault("math.maxChain", 25);
		config.addDefault("math.maxLightnings", 250);
		config.addDefault("math.maxJitter", 1);
		config.addDefault("math.jitterMod", 1.03);
		config.addDefault("math.splitChance", 0.1);
		config.addDefault("math.splitMod", 1.02);
		config.addDefault("math.distBetween", 0.2f);

		config.addDefault("general.itemType", Material.GHAST_TEAR.name());
		config.addDefault("general.itemName", "&bLightning Rod");
		config.addDefault("general.permission", "lightnings.electrocute");
		config.addDefault("general.lightningDamage", 4);
		config.addDefault("general.damageRadius", 0.1);
		config.addDefault("general.rgbStart", "9922FF");
		config.addDefault("general.rgbEnd", "7777FF");
		for(Message message : Message.values()) {
			message.addDefault();
		}
	}

	private static void setConfig(@Nonnull FileConfiguration config) {
		Preconditions.checkNotNull(config, "config");
		ConfigModel.config = config;
	}

	@Getter
	private static FileConfiguration config;

	public static int getLength() {
		return config.getInt("math.length");
	}

	public static int getLengthMod() {
		return config.getInt("math.lengthMod");
	}

	public static int getMaxChain() {
		return config.getInt("math.maxChain");
	}

	public static int getMaxLightnings() {
		return config.getInt("math.maxLightnings");
	}

	public static double getMaxJitter() {
		return config.getDouble("math.maxJitter");
	}

	public static double getJitterMod() {
		return config.getDouble("math.jitterMod");
	}

	public static double getSplitChance() {
		return config.getDouble("math.splitChance");
	}

	public static double getSplitMod() {
		return config.getDouble("math.splitMod");
	}

	public static float getDistBetween() {
		return (float) config.getDouble("math.distBetween");
	}

	public static Material getItemType() {
		return Optional.ofNullable(Material.matchMaterial(
				Strings.nullToEmpty(config.getString("general.itemType"))
		)).orElse(Material.GHAST_TEAR);
	}

	public static String getItemName() {
		return ChatColor.translateAlternateColorCodes('&',
				Strings.nullToEmpty(config.getString("general.itemName"))
		);
	}

	public static String getPermission() {
		return Strings.nullToEmpty(config.getString("general.permission"));
	}

	public static double getLightningDamage() {
		return config.getDouble("general.lightningDamage");
	}

	public static double getDamageRadius() {
		return config.getDouble("general.damageRadius");
	}

	public static int getRGBStart() {
		String hex = config.getString("general.rgbStart");
		try {
			return new BigInteger(hex, 16).intValue();
		} catch (Exception ignored) {
			return 0x9922FF;
		}
	}

	public static int getRGBEnd() {
		String hex = config.getString("general.rgbEnd");
		try {
			return new BigInteger(hex, 16).intValue();
		} catch (Exception ignored) {
			return 0x7777FF;
		}
	}

}
