package me.petomka.lightnings.util;

import com.google.common.base.Preconditions;
import me.petomka.lightnings.LightningMain;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public enum Message {

	NO_PERMISSION("no permission", "&cYou do not have permission to do this."),
	PLAYER_NOT_FOUND("player not found", "&cPlayer was not found by this name: {player}"),
	SPECIFY_PLAYER("specify player", "&cPlease specify a player!"),
	NO_FREE_INVENTORY_SLOT("no free inventory slot", "&cTarget player does not have a free space in his inventory."),
	ITEM_GIVEN("item given", "&aGave the lightning item to {player}"),
	LIGHTNINGS_RELOADING("reloading", "&aReloading lightnings..."),
	LIGHTNINGS_RELOADED("reloaded", "&aSuccessfully reloaded lightnings!");

	private static final String CONFIG_NAMESPACE = "message.";

	private final String key;
	private final String defaultMessage;

	Message(String key, String defaultMessage) {
		this.key = CONFIG_NAMESPACE + key;
		this.defaultMessage = defaultMessage;
	}

	public void addDefault() {
		LightningMain.getInstance().getConfig().addDefault(key, defaultMessage);
	}

	public String build(String... placeHolderReplacements) {
		Preconditions.checkArgument(placeHolderReplacements.length % 2 == 0,
				"placeHolderReplacements must be of even size");
		String result = LightningMain.getInstance().getConfig().getString(key, defaultMessage);
		for (int i = 0; i < placeHolderReplacements.length; i += 2) {
			result = result.replace(placeHolderReplacements[i], placeHolderReplacements[i + 1]);
		}
		return ChatColor.translateAlternateColorCodes('&', result);
	}

	public void send(CommandSender sender, String... placeHolderReplacements) {
		sender.sendMessage(build(placeHolderReplacements));
	}

}
