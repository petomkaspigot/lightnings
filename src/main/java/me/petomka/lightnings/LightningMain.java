package me.petomka.lightnings;

import lombok.Getter;
import me.petomka.lightnings.commands.LightningsCommand;
import me.petomka.lightnings.listeners.PlayerListener;
import me.petomka.lightnings.util.ConfigModel;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

public class LightningMain extends JavaPlugin {

	@Getter
	private static LightningMain instance;

	public void onEnable() {
		instance = this;
		getLogger().log(Level.INFO, "Electrocuting your server...");

		ConfigModel.addDefaults(this);
		getConfig().options().copyDefaults(true);
		saveConfig();

		PlayerListener playerListener = new PlayerListener(this);
		getServer().getPluginManager().registerEvents(playerListener, this);
		getCommand("lightnings").setExecutor(new LightningsCommand(this, playerListener));
	}

	public void onDisable() {
		getLogger().log(Level.INFO, "No more current available for lightnings!");
	}

}
