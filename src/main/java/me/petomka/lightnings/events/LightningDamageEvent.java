package me.petomka.lightnings.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.Set;

@RequiredArgsConstructor
@Getter
@Setter
public class LightningDamageEvent extends Event implements Cancellable {

	@Getter
	private static HandlerList handlerList = new HandlerList();

	private final Set<Entity> entities;
	private double damage;
	private boolean cancelled;

	@Override
	public HandlerList getHandlers() {
		return handlerList;
	}
}
