package me.petomka.lightnings.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@RequiredArgsConstructor
@Getter
@Setter
public class LightningFireEvent extends Event implements Cancellable {

	@Getter
	private static HandlerList handlerList = new HandlerList();

	private final Player player;
	private boolean cancelled;

	@Override
	public HandlerList getHandlers() {
		return handlerList;
	}

}
